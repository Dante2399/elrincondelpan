using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace ElRinconDenPan.Pages;

public partial class EditarAlmacen : ContentPage
{

    AlmacenManager almacenManager;
    Almacen Almacen;
    bool esNuevo;
    public EditarAlmacen(Almacen almacen)
	{
		InitializeComponent();
        this.BindingContext = almacen;
        this.Almacen = almacen;
        esNuevo = string.IsNullOrEmpty(almacen.Id);
        almacenManager = FabricManager.AlmacenManager();
        //List<Almacen> alma = almacenManager.ObtenerTodos;
        btnEliminar.IsVisible = !esNuevo;
    }

    private void btnGuargar_Clicked(object sender, EventArgs e)
    {
        Almacen a = this.BindingContext as Almacen;        
        //Guardar
        Almacen result = esNuevo ? almacenManager.Insertar(a) : almacenManager.Modificar(a, a.Id);
        if (result != null)
        {
            DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", almacenManager.Error, "Ok");
        }

    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Advertencia", "�Realmente quiere eliminar el producto del almacen?", "SI", "NO");
            if (r)
            {
                if (almacenManager.Eliminar(Almacen.Id))
                {
                    await DisplayAlert("Realizado", "Producto eliminado correctamente del almacen", "OK");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", almacenManager.Error, "OK");
                }
            }
        }
        catch (Exception ex)
        {

            await DisplayAlert("Error", ex.Message, "OK");
        }
    }
}