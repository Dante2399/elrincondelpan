using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace ElRinconDenPan.Pages;

public partial class EditarVentas : ContentPage
{
    Ventas Ventas;
    VentasManager ventasManager;
    bool esNuevo;
	public EditarVentas(Ventas ventas)
	{
		InitializeComponent();
        this.BindingContext = ventas;
        this.Ventas = ventas;
        esNuevo = string.IsNullOrEmpty(ventas.Id);
        ventasManager = FabricManager.VentasManager();
        List<Ventas> product = ventasManager.ObtenerTodos;
        btnEliminar.IsVisible = !esNuevo;
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Ventas v = this.BindingContext as Ventas;
        //Guardar
        Ventas result = esNuevo ? ventasManager.Insertar(v) : ventasManager.Modificar(v, v.Id);
        if (result != null)
        {
            DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", ventasManager.Error, "Ok");
        }
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Advertencia", "Seguro que quieres eliminar este venta?", "si", "no");
            if (r)
            {
                if (ventasManager.Eliminar(Ventas.Id))
                {
                    await DisplayAlert("Realizado", "Venta Eliminada Correctamente", "OK");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", ventasManager.Error, "OK");
                }
            }
        }
        catch (Exception ex)
        {

            await DisplayAlert("Error", ex.Message, "OK");
        }
    }
}