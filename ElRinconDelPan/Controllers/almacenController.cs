﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using ElRinconDelPan.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using panaderia.Controllers;
using LiteDB;
using System.Data;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlmacenController : GenericController<Almacen>
    {
        public AlmacenController():base(new AlmacenValidator())
        {
            
        }

    }
}
