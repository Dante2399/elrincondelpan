﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class VentasValidator:GenericValidator<Ventas>
    {
        public VentasValidator()
        {
            RuleFor(V => V.NombreProducto).NotEmpty().MaximumLength(50) ;
            RuleFor(V => V.Cantidad).NotEmpty().GreaterThan(0);
            RuleFor(v => v.Total).NotEmpty();
        }
    }
}
