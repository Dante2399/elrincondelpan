using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace ElRinconDenPan.Pages;

public partial class CatalogoProveedores : ContentPage
{
    ProveedoresManager proveedoresManager;

	public CatalogoProveedores()
	{
		InitializeComponent();
        proveedoresManager=FabricManager.ProveedoresManager();
	}
    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstProveedores.ItemsSource = null;
        lstProveedores.ItemsSource = proveedoresManager.ObtenerTodos;
    }

    private void lstProveedores_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarProveedores(e.SelectedItem as Proveedores));
    }

    private void btnAgregarProveedor_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarProveedores(new COMMON.Entidades.Proveedores()));
    }
}