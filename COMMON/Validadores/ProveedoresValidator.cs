﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace COMMON.Validadores
{
    public class ProveedoresValidator:GenericValidator<Proveedores>
    {
        public ProveedoresValidator()
        {
            RuleFor(p =>p.NombreProveedor).NotEmpty().MaximumLength(50);
            RuleFor(p =>p.ApellidoP).NotEmpty().MaximumLength(10);
            RuleFor(p =>p.ApellidoM).NotEmpty().MaximumLength(10);
            RuleFor(p =>p.Email).NotEmpty().MaximumLength(50);
            RuleFor(p =>p.Direccion).NotEmpty().MaximumLength(100);
        }
    }
}
