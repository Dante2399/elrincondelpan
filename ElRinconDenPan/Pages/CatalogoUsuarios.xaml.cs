using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace ElRinconDenPan.Pages;

public partial class CatalogoUsuarios : ContentPage
{
    UsuariosManager usuariosManager;
	public CatalogoUsuarios()
	{
		InitializeComponent();
        usuariosManager = FabricManager.UsuariosManager();
	}
    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstUsuarios.ItemsSource = null;
        lstUsuarios.ItemsSource = usuariosManager.ObtenerTodos;
    }

    private void lstUsuarios_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarUsuarios(e.SelectedItem as Usuarios));
    }

    private void btnAgregarUsuario_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarUsuarios(new COMMON.Entidades.Usuarios()));
    }
}