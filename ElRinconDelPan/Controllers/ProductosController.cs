﻿using COMMON.Entidades;
using COMMON.Validadores;
using ElRinconDelPan.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using panaderia.Controllers;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : GenericController<Productos>
    {
        public ProductosController():base(new ProductosValidator())
        {
            
        }
    }
}
