﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
    public class AlmacenManager:GenericManager<Almacen>
    {
        public AlmacenManager(GenericValidator<Almacen> validator):base(validator)
        {            
        }
        
    }
}
