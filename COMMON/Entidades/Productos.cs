﻿using panaderia.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Productos:Base
    {
        public string NombrePan { get; set; }
        public string Tipo { get; set; }
        public double Precio { get; set; }
    }
}
