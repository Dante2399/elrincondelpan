﻿using FluentValidation;
using panaderia.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public abstract class GenericValidator<T>:AbstractValidator<T> where T:Base
    {
        public GenericValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
        }
    }
}
