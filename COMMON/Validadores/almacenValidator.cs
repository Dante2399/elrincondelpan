﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace COMMON.Validadores
{
    public class AlmacenValidator:GenericValidator<Almacen>
    {
        public AlmacenValidator()
        {
            RuleFor(a=>a.NombreProducto).NotEmpty().MaximumLength(50);
            RuleFor(a=>a.Cantidad).NotEmpty().GreaterThan(0);
            RuleFor(a=>a.Precio).NotEmpty();
        }
    }
}
