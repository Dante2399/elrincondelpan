using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace ElRinconDenPan.Pages;

public partial class CatalogoProductos : ContentPage
{
    ProductosManager productosManager;
    public CatalogoProductos()
	{
		InitializeComponent();
        productosManager = FabricManager.ProductosManager();
    }
    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstProductos.ItemsSource = null;
        lstProductos.ItemsSource = productosManager.ObtenerTodos;
    }

    private void lstProductos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarProductos(e.SelectedItem as Productos));
    }

    private void btnAgregarProducto_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarProductos(new COMMON.Entidades.Productos()));
    }
}