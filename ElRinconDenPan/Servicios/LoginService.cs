﻿using COMMON.Modelos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElRinconDenPan.Servicios
{
    public class LoginService:ILoginRepository
    {
        public async Task<Login> Log(string Usuario, string Password)
        {
            var usuarios = new List<Login>();
            var client = new HttpClient();
            Login l = new Login()
            {
                Password = Password,
                NombreU = Usuario,
            };
            string url = "http://www.ElRinconDelPan.somee.com/api/Usuarios/Login";
            client.BaseAddress = new Uri(url);
            var requestBody = new
            {
                Usuario = l.NombreU,
                Password = l.Password
            };
            var jsonBody = JsonConvert.SerializeObject(requestBody);
            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(url, content).Result;
            if (response.IsSuccessStatusCode)
            {
                string responsecontent = response.Content.ReadAsStringAsync().Result;
                var login = JsonConvert.DeserializeObject<Login>(responsecontent);

                return login;
            }
            else
            {
                return null;
            }
        }
    }
}
