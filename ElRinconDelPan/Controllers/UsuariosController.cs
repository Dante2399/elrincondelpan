﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using ElRinconDelPan.Controllers;
using LiteDB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using panaderia.Controllers;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : GenericController<Usuarios>
    {
        public UsuariosController():base(new UsuariosValidator())
        {
        }


        [HttpPost("Login")]
        public ActionResult<Usuarios> Login([FromBody] Login login)
        {
            try
            {
                Usuarios dato;
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<Usuarios>(typeof(Usuarios).Name);
                    dato = col.Find(u => u.NombreU == login.NombreU && u.Password == login.Password ).SingleOrDefault();
                }
                if (dato != null)
                {
                    return Ok(dato);
                }
                else
                {
                    return BadRequest("Usuario y/o Contraseña incorrecta");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
