using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace ElRinconDenPan.Pages;

public partial class CatalagoVentas : ContentPage
{
    VentasManager ventasManager;
	public CatalagoVentas()
	{
		InitializeComponent();
        ventasManager = FabricManager.VentasManager();
	}

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstVentas.ItemsSource = null;
        lstVentas.ItemsSource = ventasManager.ObtenerTodos;
    }
    private void lstVentas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarVentas(e.SelectedItem as Ventas));
    }

    private void btnAgregarVenta_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarVentas(new COMMON.Entidades.Ventas()));
    }
}
