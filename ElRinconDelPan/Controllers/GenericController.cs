﻿using COMMON.Validadores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using panaderia.Controllers;
using COMMON.Entidades;
using LiteDB;
using panaderia.Entidades;

namespace ElRinconDelPan.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class GenericController<T> : ControllerBase, IGenericController<T> where T : Base
    {
        protected string DBName = "Panaderia.db";
        private GenericValidator<T> validator;


        public GenericController(GenericValidator<T> validador)
        {
            validator = validador;
        }

        protected List<M> ObtenerDatos<M>() where M : class
        {
            List<M> datos = new List<M>();
            using (var db = new LiteDatabase(DBName))
            {
                var col = db.GetCollection<M>(typeof(M).Name);
                datos = col.FindAll().ToList();
            }
            return datos;
        }

        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(string id)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    col.Delete(id);
                }
                return Ok(true);
            }
            catch (Exception)
            {
                return BadRequest(false);
            }
        }

        [HttpGet]
        public ActionResult<List<T>> Get()
        {
            try
            {
                List<T> datos = new List<T>();
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    datos = col.FindAll().ToList();
                }
                return Ok(datos);
            }
            catch (Exception)
            {

                return BadRequest(null);
            }
        }

        [HttpGet("{id}")]
        public ActionResult<T> Get(string id)
        {
            try
            {
                T dato;
                using (var db = new LiteDatabase(DBName))
                {
                    var col = db.GetCollection<T>(typeof(T).Name);
                    dato = col.FindById(id);
                }
                return Ok(dato);
            }
            catch (Exception)
            {

                return BadRequest(null);
            }
        }

        [HttpPost]
        public ActionResult<T> Post([FromBody] T value)
        {
            try
            {
                value.Id = Guid.NewGuid().ToString();
                var resultadoValidacion = validator.Validate(value);
                if (resultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        col.Insert(value);
                    }
                    return Ok(value.Id);
                }
                else
                {
                    string error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        error += item.ErrorMessage + ". ";
                    }
                    return BadRequest(error);
                }
            }
            catch (Exception)
            {
                return BadRequest(false);
            }
        }

        [HttpPut("{id}")]
        public ActionResult<T> Put(string id, [FromBody] T value)
        {
            try
            {
                var resultadoValidacion = validator.Validate(value);
                if (resultadoValidacion.IsValid)
                {
                    using (var db = new LiteDatabase(DBName))
                    {
                        var col = db.GetCollection<T>(typeof(T).Name);
                        col.Update(id, value);
                    }
                    return Ok(true);
                }
                else
                {
                    string error = "";
                    foreach (var item in resultadoValidacion.Errors)
                    {
                        error += item.ErrorMessage + ". ";
                    }
                    return BadRequest(false);
                }
            }
            catch (Exception)
            {
                return BadRequest(false);
            }
        }

    }
}
