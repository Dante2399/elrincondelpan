using BIZ.Managers;
using BIZ;
using COMMON.Entidades;

namespace ElRinconDenPan.Pages;

public partial class EditarUsuarios : ContentPage
{
    Usuarios Usuarios;
    UsuariosManager usuariosManager;
    bool esNuevo;
    public EditarUsuarios(Usuarios usuarios)
	{
		InitializeComponent();
        this.BindingContext = usuarios;
        this.Usuarios = usuarios;
        esNuevo = string.IsNullOrEmpty(usuarios.Id);
        usuariosManager = FabricManager.UsuariosManager();
        List<Usuarios> product = usuariosManager.ObtenerTodos;
        btnEliminar.IsVisible = !esNuevo;
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Usuarios u = this.BindingContext as Usuarios;
        //Guardar
        Usuarios result = esNuevo ? usuariosManager.Insertar(u) : usuariosManager.Modificar(u, u.Id);
        if (result != null)
        {
            DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", usuariosManager.Error, "Ok");
        }
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Advertencia", "Seguro que quieres eliminar este producto?", "si", "no");
            if (r)
            {
                if (usuariosManager.Eliminar(Usuarios.Id))
                {
                    await DisplayAlert("Realizado", "Producto Eliminado Correctamente", "OK");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", usuariosManager.Error, "OK");
                }
            }
        }
        catch (Exception ex)
        {

            await DisplayAlert("Error", ex.Message, "OK");
        }
    }
}