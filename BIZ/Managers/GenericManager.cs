﻿using BIZ.Tools;
using COMMON.Validadores;
using Newtonsoft.Json;
using panaderia.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BIZ.Managers
{
    public abstract class GenericManager<T> where T:Base
    {
        GenericValidator<T> validator;
        public string Error { get; set; }
        protected HttpClient client;

        public GenericManager(GenericValidator<T> validator)
        {
            this.validator = validator;
            client = new HttpClient();
            client.BaseAddress = new Uri(Params.URLApi);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }
        #region MetodosAsincronos
        private async Task<List<T>> ObtenerTodosAsync()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(typeof(T).Name).ConfigureAwait(false);
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    Error = "";
                    return JsonConvert.DeserializeObject<List<T>>(content);
                }
                else
                {
                    Error = content;
                    return null;
                }
            }
            catch (Exception ex)
            {

                Error = ex.Message;
                return null;
            }
        }

        private async Task<T> BuscarPorIdAsync(string id)
        {
            try
            {
                HttpResponseMessage reponse = await client.GetAsync($"{typeof(T).Name}/{id}").ConfigureAwait(false);
                var content = await reponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (reponse.IsSuccessStatusCode)
                {
                    Error = "";
                    return JsonConvert.DeserializeObject<T>(content);
                }
                else
                {
                    Error = content;
                    return null;
                }
            }
            catch (Exception ex)
            {

                Error = ex.Message;
                return null;
            }
        }

        private async Task<T> InsertarAsync(T entidad)
        {
            try
            {
                entidad.Id = Guid.NewGuid().ToString();
                var resultValidator = validator.Validate(entidad);
                if (resultValidator.IsValid)
                {
                    var body = new StringContent(JsonConvert.SerializeObject(entidad), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync(typeof(T).Name, body).ConfigureAwait(false);
                    var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                    {
                        Error = "";
                        return JsonConvert.DeserializeObject < T > (content);
                    }
                    else
                    {
                        Error = content;
                        return null;
                    }
                }
                else
                {
                    Error = "Ocurrio un error de validacion: ";
                    foreach (var item in resultValidator.Errors)
                    {
                        Error += $"{item.ErrorMessage}.";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {

                Error = ex.Message;
                return null;
            }
        }

        private async Task<T> ModificarAsync(T entidad, string id)
        {
            try
            {
                entidad.Id = Guid.NewGuid().ToString();
                var resultValidator = validator.Validate(entidad);
                if (resultValidator.IsValid)
                {
                    var body = new StringContent(JsonConvert.SerializeObject(entidad), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PutAsync($"{typeof(T).Name}/{id}", body).ConfigureAwait(false);
                    var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                    {
                        Error = "";
                        return JsonConvert.DeserializeObject<T>(content);
                    }
                    else
                    {
                        Error = content;
                        return null;
                    }
                }
                else
                {
                    Error = "Ocurrio un error de validacion: ";
                    foreach (var item in resultValidator.Errors)
                    {
                        Error += $"{item.ErrorMessage}. ";
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {

                Error = ex.Message;
                return null;
            }
        }

        private async Task<bool> EliminarAsync(string id)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync($"{typeof(T).Name}/{id}").ConfigureAwait(false);
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    Error = "";
                    return true;
                }
                else
                {
                    Error = content;
                    return false;
                }
            }
            catch (Exception ex)
            {

                Error = ex.Message;
                return false;
            }
        }
        #endregion
        #region MetodosSincronos
        public List<T> ObtenerTodos => ObtenerTodosAsync().Result;
        public T BuscarPorId(string id) => BuscarPorIdAsync(id).Result;
        public T Insertar(T entidad) => InsertarAsync(entidad).Result;
        public T Modificar(T entidad, string id) => ModificarAsync(entidad, id).Result;
        public bool Eliminar(string id) => EliminarAsync(id).Result;
        #endregion
    }
}
