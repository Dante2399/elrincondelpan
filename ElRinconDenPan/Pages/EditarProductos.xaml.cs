using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace ElRinconDenPan.Pages;

public partial class EditarProductos : ContentPage
{
    ProductosManager productosManager;
    Productos Productos;
    bool esNuevo;
    public EditarProductos(Productos productos)
	{
		InitializeComponent();
        this.BindingContext = productos;
        this.Productos = productos;
        esNuevo = string.IsNullOrEmpty(Productos.Id);
        productosManager = FabricManager.ProductosManager();
        List<Productos> product = productosManager.ObtenerTodos;
        btnEliminar.IsVisible = !esNuevo;
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Productos p= this.BindingContext as Productos;
        //Guardar
        Productos result = esNuevo ? productosManager.Insertar(p) : productosManager.Modificar(p, p.Id);
        if (result != null)
        {
            DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", productosManager.Error, "Ok");
        }
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Advertencia", "Seguro que quieres eliminar este producto?", "si", "no");
            if (r)
            {
                if (productosManager.Eliminar(Productos.Id))
                {
                    await DisplayAlert("Realizado", "Producto Eliminado Correctamente", "OK");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", productosManager.Error, "OK");
                }
            }
        }
        catch (Exception ex)
        {

            await DisplayAlert("Error", ex.Message, "OK");
        }
    }
}