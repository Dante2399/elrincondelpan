﻿using COMMON.Entidades;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
    public class ProveedoresManager : GenericManager<Proveedores>
    {
        public ProveedoresManager(GenericValidator<Proveedores> validator) : base(validator)
        {
            
        }
    }
}
