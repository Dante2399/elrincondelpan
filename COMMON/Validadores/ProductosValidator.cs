﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class ProductosValidator:GenericValidator<Productos>
    {
        public ProductosValidator()
        {
            RuleFor(p=>p.NombrePan).NotEmpty().MaximumLength(50);
            RuleFor(p=>p.Tipo).NotEmpty().MaximumLength(50);
            RuleFor(p=>p.Precio).NotEmpty();
        }
    }
}
