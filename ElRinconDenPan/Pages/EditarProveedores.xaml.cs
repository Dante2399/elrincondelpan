using BIZ;
using BIZ.Managers;
using COMMON.Entidades;

namespace ElRinconDenPan.Pages;

public partial class EditarProveedores : ContentPage
{
    Proveedores Proveedores;
    ProveedoresManager proveedoresManager;
    bool esNuevo;

	public EditarProveedores(Proveedores proveedores)
	{
		InitializeComponent();
        this.BindingContext = proveedores;
        this.Proveedores = proveedores;
        esNuevo = string.IsNullOrEmpty(proveedores.Id);
        proveedoresManager = FabricManager.ProveedoresManager();
        List<Proveedores> product = proveedoresManager.ObtenerTodos;
        btnEliminar.IsVisible = !esNuevo;
    }

    private void btnGuardar_Clicked(object sender, EventArgs e)
    {
        Proveedores pr = this.BindingContext as Proveedores;
        //Guardar
        Proveedores result = esNuevo ? proveedoresManager.Insertar(pr) : proveedoresManager.Modificar(pr, pr.Id);
        if (result != null)
        {
            DisplayAlert("�xito", "Datos guardados correctamente", "Ok");
            Navigation.PopAsync();
        }
        else
        {
            DisplayAlert("Error", proveedoresManager.Error, "Ok");
        }
    }

    private async void btnEliminar_Clicked(object sender, EventArgs e)
    {
        try
        {
            var r = await DisplayAlert("Advertencia", "Seguro que quieres eliminar este proveedor?", "si", "no");
            if (r)
            {
                if (proveedoresManager.Eliminar(Proveedores.Id))
                {
                    await DisplayAlert("Realizado", "Proveedor Eliminado Correctamente", "OK");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", proveedoresManager.Error, "OK");
                }
            }
        }
        catch (Exception ex)
        {

            await DisplayAlert("Error", ex.Message, "OK");
        }
    }
}