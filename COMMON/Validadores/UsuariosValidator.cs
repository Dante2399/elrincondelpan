﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class UsuariosValidator:GenericValidator<Usuarios>
    {
        public UsuariosValidator()
        {
            RuleFor(u => u.NombreU).NotEmpty().MaximumLength(50);
            RuleFor(u => u.ApellidoP).NotEmpty().MaximumLength(50);
            RuleFor(u => u.ApellidoM).NotEmpty().MaximumLength(50);
            RuleFor(u => u.Email).NotEmpty().MaximumLength(100);
            RuleFor(u => u.Password).NotEmpty().MaximumLength(10) ;
        }
    }
}
