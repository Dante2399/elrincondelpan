﻿using BIZ.Managers;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public static class FabricManager
    {
        public static AlmacenManager AlmacenManager() => new AlmacenManager(new AlmacenValidator());
        public static ProductosManager ProductosManager() => new ProductosManager(new ProductosValidator());
        public static ProveedoresManager ProveedoresManager() => new ProveedoresManager(new ProveedoresValidator());
        public static UsuariosManager UsuariosManager() => new UsuariosManager(new UsuariosValidator());
        public static VentasManager VentasManager() => new VentasManager(new VentasValidator());
    }
}
