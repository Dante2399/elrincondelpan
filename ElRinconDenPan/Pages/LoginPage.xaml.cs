using COMMON.Modelos;
using ElRinconDenPan.Servicios;

namespace ElRinconDenPan.Pages;

public partial class LoginPage : ContentPage
{
    readonly ILoginRepository _loginRepository = new LoginService();
    public LoginPage()
	{
		InitializeComponent();
	}

    private async void Button_Clicked(object sender, EventArgs e)
    {
        string usuario = txtUsuario.Text;
        string password = txtPassword.Text;

        if (usuario == null || password == null)
        {
            await DisplayAlert("Error", "Ingrese un Usuario y/o Contraseņa", "OK");
            return;
        }

        Login usuarios = await _loginRepository.Log(usuario, password);
        if (usuarios != null)
        {
            App.Current.MainPage = new AppShell();
            //await Navigation.PopAsync();
            //await Navigation.PushAsync(new AppShell());
        }
        else
        {
            await DisplayAlert("Error", "Usuario y/o Contraseņa incorrecta", "OK");
        }


    }
}