using BIZ;
using BIZ.Managers;
using COMMON.Entidades;
using COMMON.Modelos;

namespace ElRinconDenPan.Pages;

public partial class CatalogoAlmacen : ContentPage
{
    AlmacenManager almacenManager;
    List<Almacen> almacen;
    public CatalogoAlmacen()
	{
		InitializeComponent();
        almacenManager = FabricManager.AlmacenManager();
	}

    protected override void OnAppearing()
    {
        base.OnAppearing();
        lstAlmacen.ItemsSource = null;
        lstAlmacen.ItemsSource = almacenManager.ObtenerTodos;

    }

    private void lstAlmacen_ItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        Navigation.PushAsync(new EditarAlmacen(e.SelectedItem as Almacen));
    }

    private void btnAgregarProductoAlmacen_Clicked(object sender, EventArgs e)
    {
        Navigation.PushAsync(new EditarAlmacen(new COMMON.Entidades.Almacen()));
    }
}