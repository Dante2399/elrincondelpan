﻿using ElRinconDenPan.Pages;

namespace ElRinconDenPan
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new CatalogoProveedores());
            //App.Current.MainPage = new AppShell();
        }
    }
}