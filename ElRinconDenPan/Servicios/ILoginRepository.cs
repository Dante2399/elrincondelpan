﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COMMON.Modelos;

namespace ElRinconDenPan.Servicios
{
    public interface ILoginRepository
    {
        Task<Login> Log(string Usuario,string Password);
    }
}
