﻿using COMMON.Entidades;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Managers
{
    public class VentasManager: GenericManager<Ventas>
    {
        public VentasManager(GenericValidator<Ventas> validator):base(validator)
        {
            
        }
    }
}
